gnome-shell-extension-dashtodock (64-1) unstable; urgency=medium

  * New upstream version
  * Migrate to salsa subgroup for gnome shell extensions
  * Set primary maintainer to Debian GNOME Maintainers
  * Set myself as uploader
  * Remove extra README.md from binary package
  * Update standards version to 4.2.1 (no related changes)

 -- Jonathan Carter <jcc@debian.org>  Wed, 29 Aug 2018 20:55:22 +0200

gnome-shell-extension-dashtodock (63-1) unstable; urgency=medium

  * New upstream release
  * Update copyright file for new translations
  * Update compat to level 11

 -- Jonathan Carter <jcc@debian.org>  Thu, 05 Apr 2018 15:14:33 +0200

gnome-shell-extension-dashtodock (62-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.1.3

 -- Jonathan Carter <jcc@debian.org>  Mon, 08 Jan 2018 13:37:48 +0200

gnome-shell-extension-dashtodock (61-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright:
    - update copyright years for po/ja.po
    - update copyright years for po/zh_CN.po
  * Update maintainer e-mail address
  * Update standards version to 4.1.0

 -- Jonathan Carter <jcc@debian.org>  Fri, 08 Sep 2017 12:17:40 +0200

gnome-shell-extension-dashtodock (60-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcarter@linux.com>  Mon, 10 Jul 2017 10:22:45 +0200

gnome-shell-extension-dashtodock (59-3) unstable; urgency=medium

  * Don't attempt to build gschemas in rules (Closes: #867448)
  * Add Testsuite field to debian/control

 -- Jonathan Carter <jcarter@linux.com>  Thu, 06 Jul 2017 19:46:40 +0200

gnome-shell-extension-dashtodock (59-2) unstable; urgency=medium

  * Upload to unstable (no changes)

 -- Jonathan Carter <jcarter@linux.com>  Wed, 05 Jul 2017 15:41:30 +0200

gnome-shell-extension-dashtodock (59-1) experimental; urgency=medium

  * New upstream release
  * Improvements to install paths (thanks Fran Glais and Jeremy Bicha)
  * Update standards version to 4.0.0

 -- Jonathan Carter <jcarter@linux.com>  Thu, 08 Jun 2017 19:38:10 +0200

gnome-shell-extension-dashtodock (57-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcarter@linux.com>  Mon, 27 Mar 2017 12:42:33 +0200

gnome-shell-extension-dashtodock (56-1) unstable; urgency=medium

  * New upstream release
  * Remove unneeded dependency on ${shlibs:Depends}
  * Remove fix-install-path patch (accepted upstream)

 -- Jonathan Carter <jcarter@linux.com>  Tue, 07 Feb 2017 13:50:55 +0200

gnome-shell-extension-dashtodock (55-4) unstable; urgency=medium

  * Depend on gir1.2-clutter-1.0 (Closes: #848662)

 -- Jonathan Carter <jcarter@linux.com>  Mon, 23 Jan 2017 14:31:20 +0200

gnome-shell-extension-dashtodock (55-3) unstable; urgency=medium

  * Re-apply path fix patch (Closes: #844963)
  * Upgrade debian/watch to version 4

 -- Jonathan Carter <jcarter@linux.com>  Wed, 16 Nov 2016 09:07:36 +0200

gnome-shell-extension-dashtodock (55-2) unstable; urgency=medium

  * Fix install file to install compiled schema properly
  * Bump compat level to 10 (no changes)

 -- Jonathan Carter <jcarter@linux.com>  Mon, 14 Nov 2016 13:00:41 +0200

gnome-shell-extension-dashtodock (55-1) unstable; urgency=medium

  * Initial release (Closes: 829185)

 -- Jonathan Carter <jcarter@linux.com>  Mon, 03 Oct 2016 09:22:40 +0200
